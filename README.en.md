# VueMusic

#### Description
基于Vue框架的音乐播放器---口袋音乐
1、用户注册、登录验证
2、用户初次登录引导提示
3、推荐音乐、歌单、mv
4、歌单、歌曲、mv点击播放、暂停及切换，歌曲又歌词显示效果、mv有查看评论及点赞功能
5、歌曲、歌手关键字搜索有关歌曲、歌单
6、用户登录后的个人信息存储与显示

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
